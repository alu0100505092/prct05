require "Question/Simple_Choice"

module Question
   class SimpleChoice
       describe Question::SimpleChoice do
           
           before :each do 
               @q=Question::SimpleChoice.new({:test => '3+3=', :rigth => '6' , :distractors => [1,2,3]})
           end
           
           context "cuando se crea una pregunta " do
              
           
                it " debe tener tres componentes " do
                    expect(@q.text)=='3+3='
                    expect(@q.rigth)==6
                    expect(@q.distractors)==[1,2,3]
                end
                
                    it "tiene que tener texto y las demas opciones "  do
                    expect{(Question::SimpleChoice.new(:text => '5*8=?'))}.to 
                        raise_error(ArgumentEror)
                    end
                    
                    context "cuando se convierte " do
                        it " posible convertir en HTML" do
                            expect(@q).to respond_to :to_html
                        end
                    end
                    
                    
           end
       end
               
              
       
       
        
   end
 

end

